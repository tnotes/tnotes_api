import express from 'express';
import bodyParser from 'body-parser';
import database from './config/database';
const cors = require('cors');

import routes from './routes/index';

const app = express();


const configureExpress = () => {
  app.use(bodyParser.json());
  app.use(cors());
  app.use('/', routes);
  return app;
};

export default () => database.connect().then(configureExpress);
