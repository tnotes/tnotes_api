class Usuario {
  constructor(usuario) {
    this.usuario = usuario;
  };

  async salvar(req, res) {
    console.log(req.body);
    try {
      const user = this.usuario(req.body);
      await user.save();
      res.status(201).send({ msg: 'Sucess' });
    } catch (error) {
      console.log(error);
      res.status(401).send({ msg: error.message });
    }
  };

  async monstrarUsuarios(req, res) {
    try {
      const usuarios = await this.usuario.find({}, '_id nome usuario email');
      res.send(usuarios);
    } catch (error) {
      console.log(error);
      res.status(400).send('Error');
    }
  };

  async buscarUsuario(req, res) {
    const meuId = req.params.id;

    const usuario = await this.usuario.findOne({ _id: meuId }, '_id nome usuario email');
    if (usuario != null) {
      res.status(200).send(usuario);
    } else {
      res.status(400).send({ msg: 'Usuario não encontrado' });
    }
  }


  atualizarUsuario(req, res) {
    return this.usuario.findOneAndUpdate({ _id: req.params.id }, req.body)
      .then(() => res.sendStatus(200))
      .catch(err => res.status(422).send(err.message));
  };

  removerUsuario(req, res) {
    return this.usuario.deleteOne({ _id: req.params.id })
      .then(() => res.sendStatus(204))
      .catch(err => res.status(400).send(err.message));
  };
};


export default Usuario;
