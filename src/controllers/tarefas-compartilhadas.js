class TarefasCompartilhadas {
  constructor(tarefaCompartilhada, user) {
    this.tarefa = tarefaCompartilhada;
    this.usuario = user;
  };

  async adicionarTarefaCompartilhada(req, res) {
    console.log(req.body);
    try {
      const user = await this.usuario.findOne({ usuario: req.body.usuario });
      const tarefaParaMandar = {
        id_tarefa: req.body.id_tarefa,
        id_usuario: user._id,
        id_autor: req.body.id_autor,
      };
      if (user != null) {
        const tarefa = this.tarefa(tarefaParaMandar);
        await tarefa.save();
        res.status(201).send({ msg: 'Sucesso' });
      } else {
        res.status(401).send({ msg: 'Usuario não encontrado' });
      }
    } catch (err) {
      res.status(401).send(err);
    }
  }

  async pegarTarefasCompartilhada(req, res) {
    try {
      const mTarefas = await this.tarefa.find({ id_usuario: req.params.id }).populate('id_tarefa').populate('id_autor');
      res.send(mTarefas);
    } catch (err) {
      console.log(err);
    }
  }
}

export default TarefasCompartilhadas;
