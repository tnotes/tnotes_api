
class Tarefa {
  constructor(tarefa) {
    this.tarefa = tarefa;
  }

  async adicionarTarefa(req, res) {
    try {
      const taref = this.tarefa(req.body);
      await taref.save();
      res.status(201).send({ msg: 'Sucess' });
    } catch (err) {
      console.log(err);
      res.status(401).send({ msg: err.message });
    }
  }

  async pegarTarefasporUsuario(req, res) {
    try {
      const idUsuario = req.params.id;
      const tarefas = await this.tarefa.find({ autor_id: idUsuario });
      res.send(tarefas);
    } catch (err) {
      res.status(400).send(err.message);
    }
  }

  async pegarTarefasporGrupo(req, res) {
    try {
      const idGrupo = req.params.id;
      const tarefas = await this.tarefa.find({ grupo_id: idGrupo });
      res.send(tarefas);
    } catch (err) {
      res.status(400).send(err.message);
    }
  };

  async pegarTarefasporGrupoOrdenadaPorData(req, res) {
    try {
      const idGrupo = req.params.id;
      const tarefas = await this.tarefa.find({ grupo_id: idGrupo }).sort('data');
      res.send(tarefas);
    } catch (err) {
      res.status(400).send(err.message);
    }
  };

  atualizarTarefa(req, res) {
    return this.tarefa.findOneAndUpdate({ _id: req.params.id }, req.body)
      .then(() => res.sendStatus(200))
      .catch(err => res.status(422).send(err.message));
  };

  removerTarefa(req, res) {
    return this.tarefa.deleteOne({ _id: req.params.id })
      .then(() => res.sendStatus(204))
      .catch(err => res.status(400).send(err.message));
  };
};

export default Tarefa;
