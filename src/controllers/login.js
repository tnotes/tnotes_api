import passport from 'passport';
import passportJWT from 'passport-jwt';
import jwt from 'jwt-simple';
import Usuario from '../models/usuario';
import Config from '../config/config';

class Login {
  constructor() {
    this.ExtractJwt = passportJWT.ExtractJwt;
    this.Strategy = passportJWT.Strategy;
    const jwtSecret = Config.jwtSecret;
    this.params = {
      secretOrKey: jwtSecret,
      jwtFromRequest: this.ExtractJwt.fromAuthHeaderAsBearerToken('Authorization'),
    };
  };

  auth() {
    const strategy = new this.Strategy(this.params, async (payload, done) => {
      const usuario = await Usuario.findOne({ _id: payload._id });
      if (usuario) {
        return done(null, { _id: usuario._id });
      } else {
        return done(new Error('Usuário não encontrado.'), null);
      }
    });
    passport.use(strategy);
    return {
      initialize: () => passport.initialize(),
      authenticate: () => passport.authenticate('jwt', Config.jwtSession),
    };
  }

  async login(req, res) {
    if (req.body.usuario && req.body.senha) {
      const user = req.body.usuario;
      const senha = req.body.senha;
      console.log(user);
      const usu = await Usuario.findOne({ usuario: user });
      console.log(usu);
      if (usu) {
        if (usu.senha === senha) {
          const payload = { _id: usu._id, nome: usu.nome };
          const tokenGerado = jwt.encode(payload, Config.jwtSecret);
          res.json({ token: tokenGerado, id_usuario: usu._id });
        } else {
          console.log('Senha errada');
          res.status(401).send({ msg: 'Senha Incorreta' });
        }
      } else {
        console.log('Usuario não encontrado');
        res.status(401).send({ msg: 'Usuario não encontrado' });
      }
    } else {
      console.log('Algum campo vazio');
      res.status(401).send({ msg: 'Campo vazio!' });
    }
  }
}

export default Login;
