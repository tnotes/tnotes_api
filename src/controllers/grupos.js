class Grupo {
  constructor(mGrupo) {
    this.grupo = mGrupo;
  }

  async criarGrupo(req, res) {
    try {
      if (await this.verificarDisponibilidadeParaSubGrupo(req.body.sub_grupo)) {
        const mGrupo = this.grupo(req.body);
        await mGrupo.save();
        res.status(201).send({ mensage: 'Sucess' });
        console.log('deu certo');
      } else {
        res.status(401).send('Ja é subgrupo');
      }
    } catch (err) {
      console.log(err.message);
      res.status(401).send(err.message);
    }
  }

  async verificarDisponibilidadeParaSubGrupo(id) {
    if (id == null) {
      return true;
    } else {
      const mGrupo = await this.grupo.findOne({ _id: id }, 'sub_grupo');
      if (mGrupo.sub_grupo == null) {
        return true;
      } else {
        return false;
      }
    }
  }

  async pegarTodosGrupos(req, res) {
    try {
      const mId = req.params.id;
      const grupos = await this.grupo.find({ sub_grupo: null, id_usuario: mId });
      // eslint-disable-next-line no-restricted-syntax
      for (const elemento of grupos) {
        const grupo = elemento;
        const subGrupos = await this.grupo.find({ sub_grupo: grupo._id });
        grupo.subgrupos = subGrupos;
      };
      res.send(grupos);
    } catch (err) {
      res.status(400).send(err.message);
    }
  }

  async pegartodosSubgrupos(req, res) {
    try {
      const grupos = await this.grupo.find({ sub_grupo: !null });
      res.send(grupos);
    } catch (err) {
      res.status(400).send(err.message);
    }
  }

  async pegarGrupoPorId(req, res) {
    try {
      const idGrupo = req.params.id;
      const mGrupo = await this.grupo.findOne({ _id: idGrupo });
      res.send(mGrupo);
    } catch (err) {
      console.log(err.message);
      res.status(400).send(err.message);
    }
  }

  async pegarSubGrupos(req, res) {
    try {
      const idSubGrupo = req.params.id;
      const grupos = await this.grupo.find({ sub_grupo: idSubGrupo });
      res.send(grupos);
    } catch (err) {
      res.status(400).send(err.message);
    }
  };

  atualizarGrupo(req, res) {
    return this.grupo.findOneAndUpdate({ _id: req.params.id }, req.body)
      .then(() => res.sendStatus(200))
      .catch(err => res.status(422).send(err.message));
  };

  removerGrupo(req, res) {
    return this.grupo.deleteOne({ _id: req.params.id })
      .then(() => res.sendStatus(204))
      .catch(err => res.status(400).send(err.message));
  };


};

export default Grupo;
