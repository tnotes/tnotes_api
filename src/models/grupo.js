import mongoose from 'mongoose';

const schema = mongoose.Schema({

  nome: {
    type: String,
    trim: true,
    required: true,
  },

  sub_grupo: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Grupo',
    default: null,
  },
  id_usuario: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Usuario',
    required: true,
  },
  subgrupos: [],
});

const grupo = mongoose.model('Grupo', schema);

export default grupo;
