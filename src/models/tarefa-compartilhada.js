import mongoose from 'mongoose';

const schema = mongoose.Schema({
  id_tarefa: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Tarefa',
    required: true,
  },
  id_usuario: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Usuario',
    required: true,
  },
  id_autor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Usuario',
    required: true,
  },
});

const tarefaCompartilhada = mongoose.model('tarefa_campartilhada', schema);

export default tarefaCompartilhada;
