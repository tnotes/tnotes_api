import mongoose from 'mongoose';

const schema = mongoose.Schema({
  nome: {
    type: String,
    required: true,
    trim: true,
  },
  usuario: {
    type: String,
    required: true,
    trim: true,
    unique: true,
  },
  senha: {
    type: String,
    required: true,
    trim: true,
    min: 8,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true,
  },
});

const Usuario = mongoose.model('Usuario', schema);

export default Usuario;
