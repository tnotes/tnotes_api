import mongoose from 'mongoose';

const schema = mongoose.Schema({
  descricao: {
    type: String,
    trim: true,
    required: true,
  },
  data: {
    type: Date,
    trim: true,
    required: true,
  },
  prioridade: {
    type: Number,
    trim: true,
  },
  autor_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Usuario',
    required: true,
  },
  grupo_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Grupo',
    required: true,
  },
});

const tarefa = mongoose.model('Tarefa', schema);

export default tarefa;
