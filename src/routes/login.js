import express from 'express';
import LoginController from '../controllers/login';

const router = express.Router();

const login = new LoginController();

router.post('/', (req, res) => login.login(req, res));

export default router;
