import express from 'express';
import UsuarioModel from '../models/usuario';
import Usuario from '../controllers/usuarios';


const route = express.Router();
const usuario = new Usuario(UsuarioModel);

route.post('/', (req, res) => usuario.salvar(req, res));
route.get('/', (req, res) => usuario.monstrarUsuarios(req, res));
route.get('/:id', (req, res) => usuario.buscarUsuario(req, res));
route.put('/:id', (req, res) => usuario.atualizarUsuario(req, res));
route.delete('/:id', (req, res) => usuario.removerUsuario(req, res));

export default route;
