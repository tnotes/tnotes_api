import express from 'express';
import tarefaCompartilhadaModel from '../models/tarefa-compartilhada';
import TarefasCompartilhadas from '../controllers/tarefas-compartilhadas';
import UsuarioModel from '../models/usuario';

const route = express.Router();
const tarefasCompartilhadas = new TarefasCompartilhadas(tarefaCompartilhadaModel, UsuarioModel);

route.post('/', (req, res) => tarefasCompartilhadas.adicionarTarefaCompartilhada(req, res));
route.get('/:id', (req, res) => tarefasCompartilhadas.pegarTarefasCompartilhada(req, res));

export default route;
