import express from 'express';
import usuarioRouter from './usuarios';
import tarefaRouter from './tarefas';
import grupoRouter from './grupos';
import loginRouter from './login';
import tarefaCompartilhadaRouter from './tarefas-compartilhadas';
const router = express.Router();

router.get('/', (req, res) => res.send({ api: 'tNotes', version: '0.1' }));

router.use('/grupo', grupoRouter);
router.use('/usuario', usuarioRouter);
router.use('/tarefa', tarefaRouter);
router.use('/tarefa-compartilhada', tarefaCompartilhadaRouter);
router.use('/login', loginRouter);


export default router;
