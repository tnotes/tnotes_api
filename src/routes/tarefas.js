import express from 'express';
import tarefaModel from '../models/tarefa';
import Tarefa from '../controllers/tarefas';
// import Login from '../controllers/login';

const route = express.Router();
const tarefa = new Tarefa(tarefaModel);
// const mLogin = new Login();
// const auth = mLogin.auth();

route.get('/usuario/:id', (req, res) => tarefa.pegarTarefasporUsuario(req, res));
route.get('/grupo/:id', (req, res) => tarefa.pegarTarefasporGrupo(req, res));
route.post('/', (req, res) => tarefa.adicionarTarefa(req, res));
route.delete('/:id', (req, res) => tarefa.removerTarefa(req, res));
route.get('/ordenada/:id', (req, res) => tarefa.pegarTarefasporGrupoOrdenadaPorData(req, res));

export default route;
