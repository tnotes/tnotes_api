import express from 'express';
import GrupoController from '../controllers/grupos';
import GrupoModel from '../models/grupo';
// import Login from '../controllers/login';


const route = express.Router();
const grupo = new GrupoController(GrupoModel);
// const mLogin = new Login();
// const auth = mLogin.auth();

route.get('/usuario/:id', (req, res) => grupo.pegarTodosGrupos(req, res));
route.get('/:id', (req, res) => grupo.pegarGrupoPorId(req, res));
route.post('/', (req, res) => grupo.criarGrupo(req, res));
route.get('/subgrupo/:id', (req, res) => grupo.pegarSubGrupos(req, res));

export default route;
